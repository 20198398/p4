#Crear una clase que permita ingresar valores 
# enteros por teclado y nos muestre la tabla 
# de multiplicar de dicho valor. Finalizar el 
# programa al ingresar el -1.

print("---Multiplicar sin ingresar -1---")

v = int(input("Ingresar valor para calcular tabla de multiplicar: "))


for f in range(13):
	if v == -1:
		break
	else:
		print(f'{v} x {f} = {v*f}')
