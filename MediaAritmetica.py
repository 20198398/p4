import statistics as stats
#Un programa que pida al usuario 4 números, los memorice 
# (utilizando una coleccion), calcule su media aritmética 
# y después muestre en pantalla la media y los datos ingresado.
print("---Calcular Media Aritmética---") 
print("Ingresar valores:")
v1 = (int(input("No1.: ")))
v2 = (int(input("No2.: ")))
v3 = (int(input("No3.: ")))
v4 = (int(input("No4.: ")))
valores = [v1, v2, v3, v4]
print("La media aritmética de ", valores, "es: ", (stats.mean(valores)))

