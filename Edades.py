#Crear una función que reciba una serie de edades y 
# me retorne la cantidad que son mayores o iguales a 18.

def resul_legal(edadm, *edadM):
    valores =0
    if edadm >=18:
        valores=valores+1
    for m in range(len(edadM)):
        if edadM[m]>=18:
            valores=valores+1
    return valores

#Aquí las edades están guardadadas dentro del programa. De esa manera la función las encuentra y ejecuta su trabajo.
print("Cantidad de edades mayores e iguales a 18 años: ", resul_legal(21, 45, 78, 12, 56, 89, 18))