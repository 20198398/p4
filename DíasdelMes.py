#Un programa que almacene en una lista el 
# número de días que tiene cada mes (supondremos que 
# es un año no bisiesto), pida al usuario que le indique 
# un mes (1=enero, 12=diciembre) y muestre en pantalla el 
# número de días que tiene ese mes.

meses=["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"]
dias=[31, 30, 28]

dato = int(input("Insertar número del mes: "))

if dato == 1:
    print("El mes de", meses[0], "tiene", dias[0], "días.")
elif dato == 2:
    print("El mes de", meses[1], "tiene", dias[2], "días.")
elif dato == 3:
    print("El mes de", meses[2], "tiene", dias[1], "días.")
elif dato == 4:
    print("El mes de", meses[3], "tiene", dias[0], "días.")
elif dato == 5:
    print("El mes de", meses[4], "tiene", dias[0], "días.")
elif dato == 6:
    print("El mes de", meses[5], "tiene", dias[1], "días.")
elif dato == 7:
    print("El mes de", meses[6], "tiene", dias[0], "días.")
elif dato == 8:
    print("El mes de", meses[7], "tiene", dias[0], "días.")
elif dato == 9:
    print("El mes de", meses[8], "tiene", dias[1], "días.")
elif dato == 10:
    print("El mes de", meses[9], "tiene", dias[0], "días.")
elif dato == 11:
    print("El mes de", meses[10], "tiene", dias[1], "días.")
elif dato == 12:
    print("El mes de", meses[11], "tiene", dias[0], "días.")
else:
    print("Introduzca un numero entre los 12 meses del año. ")