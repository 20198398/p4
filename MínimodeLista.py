
#Crear y cargar una lista con 10 enteros por teclado. 
# Crear un programa que identifique el valor mas pequeño 
# de la lista y la posición donde se encuentra. Mostrar 
# en pantalla el resultado.

#Crear y cargar una lista con 10 enteros por teclado. 
# Crear un programa que identifique el valor mas pequeño de 
# la lista y la posición donde se encuentra. Mostrar en 
# pantalla el resultado.
valores=[]
print("--Encontrar el valor más pequeño--")
for u in range(11):
    v=int(input("Ingrese valor:"))
    valores.append(v)
    menor=valores[0]
    posicion=0
for u in range(1,11):
    if valores[u]<menor:
        menor=valores[u]
        posicion=u
        
print("El valor más pequeño: ", menor)
print("Posicion del valor en la lista: ", posicion)
