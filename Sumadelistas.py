#Crear una lista. La lista tiene que 
# tener 3 elementos. Cada elemento debe ser 
# una lista de 5 enteros. Calcular y mostrar 
# la suma de cada lista contenida en la lista 
# principal.

lp=[[48, 45, 8, 7, 11], [3, 6, 9, 15, 2], [5, 8, 10, 40, 77]]

print("A partir de la lista principal: ", lp)
s1=lp[0][0]+lp[0][1]+lp[0][2]+lp[0][3]+lp[0][4]
print("Suma de primera lista: ", s1)

s2=lp[1][0]+lp[1][1]+lp[1][2]+lp[1][3]+lp[1][4]
print("Suma de segunda lista: ", s2)

s3=lp[2][0]+lp[2][1]+lp[2][2]+lp[2][3]+lp[2][4]
print("Suma de tercera lista: ", s3)


